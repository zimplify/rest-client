<?php
    namespace Zimplify\Rest;
    use \Exception;
    use \RuntimeException;
    use \SimpleXMLElement;
    
    /**
     * the data rules within the SDK
     * @pacakge Zimplify\Rest (code 07)
     * @type instance (code 01)
     * @file Rules (code 01)
     */
    class Rules {

        const ERR_BAD_RULESET = 412070101001;
        const FLD_NAME = "name";
        const FLD_REQUIRED = "required";
        const FLD_TYPE = "type";        
        const XPATH_GET_COMMAND = "//ruleset[@command='<c>']";
        const XPATH_GET_DATA = "./param[@name='<p>']";
        const XPATH_GET_REQUIRED = "./param[@required]";

        private $conditions;

        /**
         * create the rules package
         * @param SimpleXMLElement $rules the basic rules structure
         * @return void
         */
        function __construct(SimpleXMLElement $rules) {
            $this->conditions = $rules;
        }

        /**
         * do download splicing checks
         * @param array $data the data value to check agianst
         * @param SimpleXMLElement $element the element to digest
         * @return bool
         */
        protected function inspect(array $data, SimpleXMLElement $element) : bool {
            if (count($element->children()) > 0) {
                $result = true;
                foreach ($element->children() as $node) {
                    $attributes = XmlUtils::attributes($node);
                    if (array_key_exists(self::FLD_NAME, $atttibutes)) 
                        $result = array_key_exists($attributes[self::FLD_NAME], $data) ?
                            $this->view($node, $data[$attributes[self::FLD_NAME]]) :
                            !array_key_exists(self::FLD_REQUIRED, $attributes);
                    else 
                        $result = false;

                    // if is false, just return and exit
                    if (!$result) return false;
                }
            } else 
                return true;
        }

        /**
         * this is the main method we run to validate a command
         * @param string $command the command for us to trigger
         * @param string $data (optional) the data parameters we are looking at
         * @param string $method (reference) the method we will trigger if validation pass
         * @param string $endpoint (reference) the endpoint to trigger the method if validation pass
         * @return bool 
         */
        public function validate(string $command, array $data = [], string &$method = null, string &$endpoint = null) : bool {
            $result = true;
            $requires = true;

            // make sure we have our rule set
            $rules = $this->conditions->xpath(str_replace("<c>", $command, self::XPATH_GET_COMMAND));
            if (count($rules) > 0) {
                $rules = $rules[0];
                
                // first we need to make sure the required are taken care of
                foreach ($rules->xpath(self::XPATH_GET_REQUIRED) as $required) {
                    $attributes = XmlUtils::attributes($required);
                    if (array_key_exists(self::FLD_NAME, $attributes))
                        if (!array_key_exists($attributes[self::FLD_NAME], $data)) {
                            $requires = false;
                            break;
                        }
                }

                // if we clear the required, now we have to validate the data type
                if ($requires === true) {
                    foreach ($data as $field => $value) 
                        if (count($param = $rules->xpath(str_replace("<p>", $field, self::XPATH_GET_DATA))) > 0) 
                            if (($result = $this->view($param[0], $value)) !== true)
                                break;
                        else {
                            $result = false;
                            break;
                        }                    
                } else 
                    $result = false;
            } else  
                $result = false;
            
            // return the result
            return $result;
        }

        protected function view(SimpleXMLElement $element, $value) : bool {
            $result = true;
            $attributes = XmlUtils::attributes($element);

            // loading the type
            if (array_key_exists(self::FLD_TYPE, $attributes)) {

                // here is the real work
                switch ($attrbutes[self::FLD_TYPE]) {
                    case self::DTYPE_ARRAY: $result = is_array($value) && (is_numeric(array_key_first) || count($value) == 0); break;
                    case self::DTYPE_BOOLEAN: $result = is_bool(value); break;
                    case self::DTYPE_DATE: $result = is_object($value) && is_a($value, "\\DateTime"); break;
                    case self::DTYPE_DECIMAL: $result = is_double($value); break;
                    case self::DTYPE_MAP: 
                        $result = is_array($value) && (!is_numeric(array_key_first($value)) || count($value) == 0) && $this->inspect($value, $element); 
                        break;
                    case self::DTYPE_NUMBER: $result = is_numeric($value); break;
                    case self::DTYPE_TEXT: $result = is_string($value); break;
                }
            } else 
                throw new RuntimeException("Rules is not properly formatted at field ".$attributes[self::FLD_NAME].".", self::ERR_BAD_RULESET);

            // now return the answer
            return $result;
        }        
    }