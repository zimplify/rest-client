<?php
    namespace Zimplify\Rest\Providers;
    use Zimplify\Rest\{Rules, RestBadRequestException, RestResponseException};
    use Zimplify\Core\{Provider};
    use Unirest\Request;
    use Unirest\Request\Body;
    use \RuntimeException;

    /**
     * the main provider to give us access to rest
     * @package Zimplify\Rest (code 07)
     * @type Provider (code 03)
     * @file RestRequestProvider (code 01)
     */
    class RestRequestProvider extends Provider {

        const ARGS_RULES = "rules";
        const ARGS_POST_EXEC = "callback";

        private $rules;

        /**
         * startup initializer for the service
         * @return void
         */
        protected function initialize() {
            $this->rules = new Rules($this->get(self::ARGS_RULES));
        }

        /**
         * check if all startup arguments are available
         * @return bool
         */
        protected function isRequired() : bool {
            return !is_null($this->get(self::ARGS_RULES));
        }

        /**
         * the main function of provider
         * @param string $command the command to run
         * @param array $data (optional) a hashmap for data variables
         * @param array $files (optional) a hashmap for upload files
         * @return mixed
         */
        public function run(stirng $command, array $data = [], $files = []) {
            $method = $endpoint = null;

            // first clear the ruleset to begin
            if ($this->rules->validate($command, $data, $method, $endpoint, $encoding)) {
                $body = null;

                // dealing with the form encoding
                switch ($encoding) {
                    case "form": $body = Body::form($data); break;
                    case "multipart": $body = Body::multipart($data, $files); break;
                    default: $body = Body::json($data);
                }

                // now first the request
                $response = forward_static_call(array(self::CLS_UNI_REQUEST, $method), $endpoint, $header, $body);

                // our response code checks
                if ($response->code >= 200 && $response < 300) {

                    // invoke the callback if is defined.
                    $encoder = $this->get(self::FLD_BOUNCER);
                    if ($encoder && is_callable($encoder)) $encoder($response);
                    
                    // now patch the body and return
                    $result = $response->body;
                    return $result;
                } else 
                    throw new RestResponseException($endpoint. " failed with exception: ".$response->body, $response->code);
            } else 
                throw new RestBadRequestException("Failed to validate command.");
        }
    }