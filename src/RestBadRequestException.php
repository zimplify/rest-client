<?php
    namespace Zimplify\Rest;
    use Zimplify\Communication\Intetfaces\IResponseCodeInterface;
    use \Exception;

    /**
     * Exceptions during REST request did not pass validation attempt
     * @package Zimplify\Rest (code 07)
     * @type exception (code 11)
     * @file RestBadRequestException (code 03)
     */
    class RestBadRequestException extends Exception implements IResponseCodeInterface {

        /**
         * starting up the instance
         * @param string $message (optional) the message to let the user know what happened
         * @param Throwable $previous (optional) the exception triggered this one.
         * @return void
         */
        function __construct (string $message = "" , Throwable $previous = null) {
            parent::__construct($message, self::RES_BAD_REQUEST, $previous);
        }        
    }