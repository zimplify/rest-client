<?php
    namespace Zimplify\Rest;
    use Zimplify\Communication\Intetfaces\IResponseCodeInterface;
    use \Exception;

    /**
     * Exceptions during REST request did not get 2XX from REST Service
     * @package Zimplify\Rest (code 07)
     * @type exception (code 11)
     * @file RestResponseException (code 02)
     */
    class RestResponseException extends Exception implements IResponseCodeInterface {

        /**
         * starting up the instance
         * @param string $message (optional) the message to let the user know what happened
         * @param Throwable $previous (optional) the exception triggered this one.
         * @return void
         */
        function __construct (string $message = "" , int $code = self::RES_INTERNAL_ERROR, Throwable $previous = null) {
            parent::__construct($message, $code, $previous);
        }        
    }